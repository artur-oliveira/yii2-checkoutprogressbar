CheckOutProgressBar
=============
CheckOutProgressBar

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist arturoliveira/yii2-checkoutprogressbar "*"
```

or add

```
"arturoliveira/yii2-checkoutprogressbar": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```
<?= \arturoliveira\CheckOutProgressBar::widget([
  'current_step' => 1,
  'current_step_done' => TRUE, # Optional if you want this step to be checked
  'steps' => [
      [
        'label' => 1,
        'title' => 'Step 1',
        'url' => Url::toRoute('/step1'), # Optional if you want the label and title to be clickable
      ],
      [
        'label' => 2,
        'title' => 'Step 2',
        'url' => Url::toRoute('/step2'), # Optional if you want the label and title to be clickable
      ]
    ]
  ]);
  ?>
```