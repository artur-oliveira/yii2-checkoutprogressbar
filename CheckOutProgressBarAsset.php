<?php

/**
 * @package yii2-checkoutprogressbar
 * @version 1.0.0
 */

namespace arturoliveira\checkoutprogressbar;

use yii\web\AssetBundle;

/**
 * Asset bundle for CheckProgressbar Widget
 *
 * @author Artur Oliveira <artur.oliveira@gmail.com>
 * @since 1.0
 */
class CheckOutProgressBarAsset extends AssetBundle
{
    public function init(){
      $this->sourcePath = __DIR__ . '/assets';
      $this->css = [
        'css/CheckOutProgressBar.css'
      ];
      parent::init();
    }
}
